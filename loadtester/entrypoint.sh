#!/bin/bash
while true; do
  seq $(( $RANDOM % $CONCURRENT_CALLS + 1 )) | parallel -n0 "curl -s $DEST_URL"
  sleep 1
done