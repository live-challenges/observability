-- ---------------------------------------------------------
-- Overview
-- ---------------------------------------------------------

-- Services
SELECT
    service_name AS "Service",
    COUNT(*)::numeric / (30 * 60) AS "Requests",
    AVG(duration_ms) AS "Avg Duration",
    ROUND(approx_percentile(0.90, percentile_agg(duration_ms))::numeric, 3) AS "p90 Duration",
    (count(*) filter (where status_code = 'error')::numeric / count(*)) AS "Error rate"
FROM ps_trace.span s
WHERE start_time > NOW() - INTERVAL '30m'
AND (span_kind = 'server' OR parent_span_id is NULL)
GROUP BY 1
ORDER BY 2

-- Slowest requests
SELECT
  replace(trace_id::text, '-'::text, ''::text) as "Trace ID",
  service_name as "Service",
  span_name as "Operation",
  start_time as "Time",
  duration_ms as "Duration" 
FROM ps_trace.span
WHERE start_time > NOW() - INTERVAL '30m'
AND parent_span_id is null
ORDER BY duration_ms DESC
LIMIT 50
;

-- Most common errors
SELECT
  status_message as "Error",
  service_name as "Service",
  count(*) as "Occurrences" 
FROM ps_trace.span
WHERE start_time > NOW() - INTERVAL '30m'
AND status_code = 'error'
GROUP BY 1, 2
ORDER BY 3
;

-- ---------------------------------------------------------
-- Service Details
-- ---------------------------------------------------------
-- Requests
SELECT
    time_bucket_gapfill('$__interval', start_time) AS time,
    coalesce(count(*)::numeric / (EXTRACT(epoch FROM '$__interval'::interval)), 0) AS "Requests"
FROM ps_trace.span s
WHERE $__timeFilter(start_time)
AND (span_kind = 'server' OR parent_span_id is NULL)
AND service_name = '${service}'
GROUP BY 1
ORDER BY 1
;

-- Duration
SELECT
    time_bucket_gapfill('$__interval', start_time) AS time,
    COALESCE(ROUND(approx_percentile(0.99, percentile_agg(duration_ms))::numeric, 3), 0) as "p99",
    COALESCE(ROUND(approx_percentile(0.90, percentile_agg(duration_ms))::numeric, 3), 0) as "p90",
    COALESCE(ROUND(approx_percentile(0.50, percentile_agg(duration_ms))::numeric, 3), 0) as "p50",
    COALESCE(AVG(duration_ms), 0) as "Average"
FROM ps_trace.span s
WHERE $__timeFilter(start_time)
AND (span_kind = 'server' OR parent_span_id is NULL)
AND service_name = '${service}'
GROUP BY 1
ORDER BY 1
;

-- Error rate
SELECT
    time_bucket('$__interval', start_time) as time,
    coalesce(count(*) filter (where status_code = 'error')::numeric / count(*), 0) as "Error rate"
FROM ps_trace.span s
WHERE $__timeFilter(start_time)
AND (span_kind = 'server' OR parent_span_id is NULL)
AND service_name = '${service}'
GROUP BY 1
ORDER BY 1
;

-- Stats by operation
SELECT
    span_name as "Operation",
    count(*)::numeric / (${__to:date:seconds} - ${__from:date:seconds}) AS "Requests",
    sum(duration_ms) / count(*)::numeric as "Avg Duration",
    coalesce((count(*) filter (where status_code = 'error')::numeric / count(*)), 0) as "Error rate"
FROM ps_trace.span s
WHERE $__timeFilter(start_time)
AND (span_kind = 'server' OR parent_span_id is NULL)
AND service_name = '${service}'
GROUP BY 1
ORDER BY 1
;

-- Slowest operations 
SELECT
  replace(trace_id::text, '-'::text, ''::text) as "Trace ID",
  span_name as "Operation",
  start_time as "Time",
  duration_ms as "Duration"
FROM ps_trace.span
WHERE $__timeFilter(start_time)
AND (span_kind = 'server' OR parent_span_id is NULL)
AND service_name = '${service}'
ORDER BY duration_ms DESC
LIMIT 50
;

-- Most common errors
SELECT
  status_message as "Error",
  count(*) as "Occurrences"
FROM ps_trace.span
WHERE $__timeFilter(start_time) AND
status_code = 'error' AND
service_name = '${service}'
GROUP BY 1
ORDER BY 2 DESC
;
