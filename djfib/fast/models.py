from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

class Fib(models.Model):
    n = models.PositiveIntegerField(default=2, validators=[MinValueValidator(2), MaxValueValidator(100)])
    val = models.PositiveIntegerField()

    def __init__(self, n):
        self.n = n
        self.val = self.calculate(self.n)

    def __str__(self):
        return f"n:{self.n}, val:{self.val}"
    
    def calculate(self, n):
        nth_fib = [0] * (n + 2)
        nth_fib[1] = 1
        for i in range(2, n + 1):
            nth_fib[i] = nth_fib[i - 1] + nth_fib[i - 2]
        return nth_fib[n]