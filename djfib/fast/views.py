from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from slow.models import Fib
from .serializers import FibSerializer

from opentelemetry import trace
from opentelemetry import metrics


class FibDetailApiView(APIView):
    tracer = trace.get_tracer(__name__)
    meter = metrics.get_meter(__name__)
    call_counter = meter.create_counter(
        "fast_call_counter",
        description="The number of calculations done in a FAST way",
    )

    def get(self, request, n, *args, **kwargs):
        with self.tracer.start_as_current_span("calc_fast_fib") as rollspan: 
            fib = Fib(n)
            rollspan.set_attribute("fib.value", fib.val)
            # This adds 1 to the counter for the given roll value
            self.call_counter.add(1, {"fib.value": fib.val})
            
            serializer = FibSerializer(fib)
            return Response(serializer.data, status=status.HTTP_200_OK)
