"""
WSGI config for fib project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
from opentelemetry.instrumentation.wsgi import OpenTelemetryMiddleware
from opentelemetry.instrumentation.django import DjangoInstrumentor
from django.core.wsgi import get_wsgi_application

from uwsgidecorators import postfork
from opentelemetry import trace
from opentelemetry import metrics
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader, ConsoleMetricExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter

@postfork
def init_tracing():
    try:
        OTEL_COLLECTOR_ENDPOINT = os.environ["OTEL_COLLECTOR_ENDPOINT"]
        span_exporter = OTLPSpanExporter(endpoint=OTEL_COLLECTOR_ENDPOINT)
        metrics_exporter = OTLPMetricExporter(endpoint=OTEL_COLLECTOR_ENDPOINT)
    except Exception as e: 
        print(e)
        span_exporter = ConsoleSpanExporter()
        metrics_exporter = ConsoleMetricExporter()
        
    resource = Resource.create(attributes={"service.name": "django-app"})
    provider = TracerProvider(resource=resource)
    processor = BatchSpanProcessor(span_exporter)
    provider.add_span_processor(processor)
    trace.set_tracer_provider(provider)
    reader = PeriodicExportingMetricReader(metrics_exporter)
    provider = MeterProvider(resource=resource, metric_readers=[reader])
    metrics.set_meter_provider(provider)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fib.settings')
application = get_wsgi_application()
application = OpenTelemetryMiddleware(application)
DjangoInstrumentor().instrument(is_sql_commentor_enabled=True)