from django.conf.urls import url
from django.urls import path
from .views import FibDetailApiView

urlpatterns = [
    path('slow/<int:n>/', FibDetailApiView.as_view()),
]