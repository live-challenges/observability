from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

class Fib(models.Model):
    n = models.PositiveIntegerField(default=2, validators=[MinValueValidator(2), MaxValueValidator(100)])
    val = models.PositiveIntegerField()

    def __init__(self, n):
        self.n = n
        self.val = self.calculate(self.n)

    def __str__(self):
        return f"n:{self.n}, val:{self.val}"
    
    def calculate(self, n):
        if n <= 1:
            return n
        return self.calculate(n - 1) + self.calculate(n - 2)