from rest_framework import serializers
from .models import Fib

class FibSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fib
        fields = ["n", "val"]