# These are the necessary import declarations
from opentelemetry import trace
from opentelemetry import metrics
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.instrumentation.requests import RequestsInstrumentor
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader, ConsoleMetricExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter
import os
import requests
from random import randint
from fastapi import FastAPI, status
from fastapi.responses import JSONResponse


FAST_FIB_ENDPOINT = os.environ.setdefault("FAST_FIB_ENDPOINT","http://localhost:5000/api/fast/")
SLOW_FIB_ENDPOINT = os.environ.setdefault("SLOW_FIB_ENDPOINT","http://localhost:5000/api/slow/")
try:
    OTEL_COLLECTOR_ENDPOINT = os.environ["OTEL_COLLECTOR_ENDPOINT"]
    span_exporter = OTLPSpanExporter(endpoint=OTEL_COLLECTOR_ENDPOINT)
    metrics_exporter = OTLPMetricExporter(endpoint=OTEL_COLLECTOR_ENDPOINT)
except:
    span_exporter = ConsoleSpanExporter()
    metrics_exporter = ConsoleMetricExporter()

app = FastAPI()

# Service name is required for most backends
resource = Resource(attributes={
    SERVICE_NAME: "dice-roller"
})

provider = TracerProvider(resource=resource)
processor = BatchSpanProcessor(span_exporter)
provider.add_span_processor(processor)
trace.set_tracer_provider(provider)
reader = PeriodicExportingMetricReader(metrics_exporter)
provider = MeterProvider(resource=resource, metric_readers=[reader])
metrics.set_meter_provider(provider)

tracer = trace.get_tracer(__name__)
meter = metrics.get_meter(__name__)

# Now create a counter instrument to make measurements with
roll_counter = meter.create_counter(
    "roll_counter",
    description="The number of rolls by roll value",
)

@app.get("/", status_code=status.HTTP_200_OK)
async def root():
    value = do_roll()
    st_code = status.HTTP_200_OK
    fib_endpoint = os.environ["FAST_FIB_ENDPOINT"]
    if value < 1:
        st_code=status.HTTP_500_INTERNAL_SERVER_ERROR
    if value % 2 == 0:
        fib_endpoint = os.environ["SLOW_FIB_ENDPOINT"]
    resp = requests.get(f"{fib_endpoint}{value}",timeout=1)
    return JSONResponse(status_code=st_code, content=resp.text)

def do_roll():
    with tracer.start_as_current_span("do_roll") as rollspan:  
        res = randint(0, 6)
        rollspan.set_attribute("roll.value", res)
        # This adds 1 to the counter for the given roll value
        roll_counter.add(1, {"roll.value": res})
        return res

FastAPIInstrumentor.instrument_app(app)
RequestsInstrumentor().instrument()